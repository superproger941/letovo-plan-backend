FROM webdevops/php-nginx:7.4-alpine

RUN apk add ldb-dev libldap openldap-dev
RUN /usr/local/bin/docker-php-ext-configure ldap
RUN /usr/local/bin/docker-php-ext-install ldap

RUN apk add bind-tools
RUN apk add openrc

COPY ./ /var/www/html
WORKDIR /var/www/html

RUN chown -R application:application /var/www/html/
RUN chmod 777 -R /var/www/html/

RUN apk add php-ldap

RUN composer install
RUN php artisan config:clear
RUN php artisan cache:clear
RUN php artisan key:generate
RUN php artisan l5-swagger:generate
