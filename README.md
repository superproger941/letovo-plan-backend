1) composer install
2) php artisan migrate
3) php artisan l5-swagger:generate

To publish swagger conf:
```
php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
```

#OTHER
swagger - /api/documentation

#DOCKER
docker-compose up
