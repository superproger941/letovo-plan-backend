<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\StudentsController;
use App\Http\Controllers\Api\YearsController;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('me', [AuthController::class, 'user']);
    });
});

Route::prefix('students')->group(function () {
    Route::get('/getPlanningPageByAnalyticsId/{id}', [StudentsController::class, 'getPlanningPageByAnalyticsId']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/', [StudentsController::class, 'getStudents']);

        Route::middleware(['student.belongs.user'])->group(function () {
            Route::get('/{student}', [StudentsController::class, 'findOne']);
            Route::get('/{student}/getPlanningPage', [StudentsController::class, 'getPlanningPage']);
            Route::post('/{student}/saveActivities', [StudentsController::class, 'saveActivities']);
            Route::get('/{student}/getGraphs', [StudentsController::class, 'getGraphs']);
        });
    });
});

Route::prefix('years')->group(function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/', [YearsController::class, 'getAll']);
    });
});


