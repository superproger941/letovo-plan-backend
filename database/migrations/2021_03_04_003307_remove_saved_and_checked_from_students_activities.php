<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSavedAndCheckedFromStudentsActivities extends Migration
{
    public function up()
    {
        Schema::table('students_activities', function (Blueprint $table) {
            $table->dropColumn('checked');
            $table->dropColumn('saved');
        });
    }

    public function down()
    {
    }
}
