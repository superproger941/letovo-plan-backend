<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropYearsManageReferences extends Migration
{
    public function up()
    {
        Schema::dropIfExists('years_manage_references');

        Schema::table('students_activities', function (Blueprint $table) {
            $table->unsignedInteger('year_id')->nullable();

            $table->foreign('year_id')->references('id')->on('years');
        });
    }

    public function down()
    {
        //
    }
}
