<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropStudentsManageReferences extends Migration
{

    public function up()
    {
        Schema::dropIfExists('students_manage_references');
    }

    public function down()
    {
        //
    }
}
