<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorManageReferences extends Migration
{
    public function up()
    {
        Schema::table('manage_references', function (Blueprint $table) {
            $table->string('color');
        });
    }

    public function down()
    {
    }
}
