<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentFieldToStudentsActivities extends Migration
{
    public function up()
    {
        Schema::table('students_activities', function (Blueprint $table) {
            $table->string('comment')->nullable();
        });
    }

    public function down()
    {
    }
}
