<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManageReferenceTable extends Migration
{
    public function up()
    {
        Schema::create('manage_references', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('analytics_id')->nullable();
            $table->string('title');
            $table->timestamps();
        });
        Schema::create('students', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('full_name');
            $table->integer('analytics_id')->nullable();
            $table->integer('grade')->default(1)->nullable();
            $table->timestamps();
        });
        Schema::create('students_manage_references', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('manage_reference_id');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('manage_reference_id')->references('id')->on('manage_references');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('manage_references');
    }
}
