<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsActivitiesTable extends Migration
{
    public function up()
    {
        Schema::create('students_activities', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();

            $table->unsignedInteger('student_id');
            $table->unsignedInteger('activity_id');

            $table->unsignedInteger('value');
            $table->boolean('checked');
            $table->unsignedInteger('count');
            $table->boolean('saved');

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('students_activities');
    }
}
