<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYearsTable extends Migration
{
    public function up()
    {
        Schema::create('years', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('analytics_id')->nullable();
            $table->integer('years_start');
            $table->integer('years_end');
            $table->timestamps();
        });
        Schema::create('years_manage_references', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('manage_reference_id');
            $table->foreign('year_id')->references('id')->on('years');
            $table->foreign('manage_reference_id')->references('id')->on('manage_references');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('years');
    }
}
