<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ManageReference extends Model
{
    use Notifiable;

    protected $fillable = [
        'title',
        'analytics_id',
        'color'
    ];

    public function years()
    {
        return $this->belongsToMany(Year::class, 'years_manage_references');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }
}
