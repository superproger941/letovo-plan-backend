<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Year extends Model
{
    use Notifiable;

    protected $fillable = [
        'analytics_id',
        'years_start',
        'years_end',
    ];

    public function manageReference()
    {
        return $this->hasOne(ManageReference::class);
    }

    public function manageReferences()
    {
        return $this->belongsToMany(
            ManageReference::class,
            'years_manage_references',
        );
    }

    public function manageReferenceWithYears()
    {
        return $this->hasOne(ManageReference::class)->with('years');
    }
}
