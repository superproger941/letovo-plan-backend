<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{
    use Notifiable;

    protected $fillable = [
        'full_name',
        'analytics_id',
        'grade',
        'user_id'
    ];

    public function activities()
    {
        return $this
            ->belongsToMany(Activity::class, 'students_activities')
            ->withPivot(['count', 'value'])
            ->withTimestamps();
    }

    public function activitiesWithoutPivot()
    {
        return $this
            ->belongsToMany(Activity::class, 'students_activities')
            ->withPivot([]);
    }

    public function activitiesWithManagesReference()
    {
        $year = Year::query()->where(['years_end' => date('Y')])->first();

        return $this
            ->belongsToMany(Activity::class, 'students_activities')
            ->wherePivot('year_id', '=', $year->id)
            ->with('manageReference')
            ->withPivot(['count', 'value']);
    }

    public static function getByAnalyticsId($id)
    {
        return self::query()
            ->with('activitiesWithManagesReference')
            ->where(['analytics_id' => $id])
            ->first();
    }

    public static function createNewStudentFromRemoteData(User $user, $student): Student
    {
        $filteredStudent = new self();
        $filteredStudent->id = $student['id'];
        $filteredStudent->full_name = $student['full_name'];
        $filteredStudent->analytics_id = $student['id'];
        $filteredStudent->grade = $student['grade'];
        $filteredStudent->user_id = $user->id;
        $filteredStudent->save();

        return $filteredStudent;
    }

    public function getStudentWithActivity($activityName, $mrId, $yearsStart)
    {
        $year = Year::query()->where(['years_start' => $yearsStart])->first();
        return $this
            ->activities()
            ->withPivot(['value', 'count', 'comment', 'year_id'])
            ->wherePivot('year_id', '=', $year->id)
            ->where(['name' => $activityName])
            ->where(['manage_reference_id' => $mrId])
            ->first();
    }
}
