<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Activity extends Model
{
    use Notifiable;

    protected $fillable = [
        'name',
        'manage_reference_id',
    ];

    public function manageReference()
    {
        return $this->belongsTo(ManageReference::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'students_activities');
    }

    public function years()
    {
        return $this
            ->belongsToMany(Year::class, 'students_activities')
            ->withPivot(['year_id']);
    }
}
