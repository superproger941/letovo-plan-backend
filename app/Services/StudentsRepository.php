<?php

namespace App\Services;

use App\Models\Student;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class StudentsRepository
{
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://service.letovo.ru/']);
    }

    public function getStudents()
    {
        $userId = Auth::guard('api')->user()->id;
        $user = User::query()->where(['id' => $userId])->first();

        $students = $this->requestRemoteUsers($user);

        return $this->filterStudents($user, $students);
    }

    private function requestRemoteUsers($user)
    {
        $usernameBeforeSymbol = (explode('@', $user->username))[0];

        $response = $this->client->get(
            '/api/info/tutor/' . $usernameBeforeSymbol,
            [
                'headers' => [
                    'Authorization' => ActivitiesRepository::BEARER
                ]
            ]
        );
        $contents = json_decode($response->getBody()->getContents(), true);

        return $contents['data']['students'];
    }

    private function filterStudents($user, $students)
    {
        $filteredStudents = [
            'data' => [
                'students' => []
            ]
        ];

        foreach ($students as $student) {
            $filteredStudent = Student::getByAnalyticsId($student['id']);
            if (!empty($filteredStudent)) {
                $filteredStudent->user_id = $user->id;
                $filteredStudent->save();
                $filteredStudent->refresh();
                $filteredStudents['data']['students'][] = $filteredStudent;
            } else {
                $filteredStudents['data']['students'][] = Student::createNewStudentFromRemoteData($user, $student);
            }
        }

        return json_encode($filteredStudents);
    }
}
