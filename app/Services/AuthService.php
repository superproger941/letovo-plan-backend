<?php

namespace App\Services;

use Adldap\Auth\BindException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthService
{
    public function login($request)
    {
        $message = 'Login or password is incorrect';

        try {
            if (Auth::attempt([
                'username' => $request->username,
                'password' => $request->password,
            ], $request->rememberMe ?? false)) {

                $request->user()->forceFill([
                    'api_token' => hash('sha256', Str::random(80)),
                ])->save();

                return Auth::user();
            }
        } catch (BindException $e) {
            $message = $e->getMessage();
        }

        return [
            'message' => [
                $message
            ],
        ];
    }
}
