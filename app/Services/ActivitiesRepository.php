<?php

namespace App\Services;

use App\Models\Activity;
use App\Models\ManageReference;
use App\Models\Student;
use App\Models\Year;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivitiesRepository
{
    const ACTIVITIES_URI = '/api/refs/diplomaletovo/activities';
    const MEDALS_URI = '/api/letovodiploma';
    const RESULT_TYPE_ACTIVITY = 'activity';

    const BEARER = 'Bearer 285024b61097cdcd034900c79fb761a2727f7726b16f7ce09d8c20c7e81c705d';

    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://service.letovo.ru/']);
    }

    public function getRemoteActivitiesAndMergeWithLocal(Student $student, $yearsStart): array
    {
        $manages = $this->requestRemoteActivities();

        return $this->filterActivitiesWithManages($student, $manages, $yearsStart);
    }

    private function requestRemoteActivities()
    {
        $response = $this->client->get(
            self::ACTIVITIES_URI,
            [
                'headers' => [
                    'Authorization' => self::BEARER
                ]
            ]
        );
        $contents = json_decode($response->getBody()->getContents(), true);

        return $contents['data'];
    }

    private function filterActivitiesWithManages($student, $manages, $yearsStart): array
    {
        $managesFiltered = [];

        foreach ($manages as $index => $manage) {
            $managesFiltered = $this->setUpManageReference($index, $manage, $managesFiltered);

            $filteredActivities = $this->filterActivities($index, $student, $manage, $yearsStart);

            //request medals
            $response = $this->client->get(
                self::MEDALS_URI . '/' . $student->analytics_id . '/' . $manage['development_program_id'],
                [
                    'headers' => [
                        'Authorization' => self::BEARER
                    ]
                ]
            );
            $contents = json_decode($response->getBody()->getContents(), true);

            foreach ($contents['data'] as $item) {
                if ($item['result_type'] === self::RESULT_TYPE_ACTIVITY) {
                    $parts = explode('-', $item['student_activity']['activity_time']);
                    $year = $parts[0];
                    $filteredActivities = array_map(function ($obj) use ($item, $yearsStart, $year) {
                        $criterion = $item['letovodiploma_criterion'];
                        $idDiplom = $criterion['id_diplom'];
                        if ($obj['id'] === $idDiplom && $yearsStart === $year) {
                            if (empty($obj['constant_count'])) {
                                $obj['constant_count'] = 0;
                            }
                            $obj['constant_count'] += 1;//$item['result_score'];
                            $obj['medal'] = $criterion['diplom_level'];
                            $obj['saved'] = $obj['checked'] = true;
                        }

                        return $obj;
                    }, $filteredActivities);
                }
            }

            $managesFiltered[$index]['activities'] = $filteredActivities;
        }


        return $managesFiltered;
    }

    private function setUpManageReference($index, $manage, $managesFiltered): array
    {
        $localMr = ManageReference::query()->where(['id' => $index + 1])->first();

        if (!$localMr) {
            $localMr = $this->createNewManageReference($manage);
        }

        $managesFiltered[$index]['contentShow'] = false;
        $managesFiltered[$index]['savedShow'] = false;
        $managesFiltered[$index]['name_rus'] = $manage['name_rus'];
        $managesFiltered[$index]['color'] = $localMr->color;
        $managesFiltered[$index]['analytics_id'] = $manage['development_program_id'];

        return $managesFiltered;
    }

    private function createNewManageReference($manage): ManageReference
    {
        $localMr = new ManageReference();
        $localMr->id = $manage['development_program_id'];
        $localMr->analytics_id = $manage['development_program_id'];
        $localMr->title = $manage['name_rus'];
        $localMr->color = '#44BCC8';
        $localMr->save();
        $localMr->refresh();

        return $localMr;
    }

    private function filterActivities($index, Student $student, $manage, $yearsStart): array
    {
        $filteredActivities = [];

        foreach ($manage['activities'] as $activity) {
            $studentWithActivity = $student->getStudentWithActivity($activity['name'], $index + 1, $yearsStart);

            $activity['saved'] = $activity['checked'] = !!$studentWithActivity;
            $activity['value'] = $activity['score'] > 1 ? intval($activity['score']) : $activity['score'];

            if ($studentWithActivity) {
                $activity['count'] = $studentWithActivity->pivot->count;
                $activity['comment'] = $studentWithActivity->pivot->comment;
                $activity['created_at'] = $studentWithActivity->pivot->created_at;
            } else {
                $activity['count'] = 0;
                $activity['comment'] = '';
            }

            $filteredActivities[] = $activity;
        }

        return $filteredActivities;
    }

    public function saveActivities(Student $student, Request $request)
    {
        $manages = $request->activities;

        $student
            ->activities()
            ->wherePivot('year_id', '=', $request->year_id)
            ->detach();

        foreach ($manages as $index => $activities) {
            $mrId = $index + 1;

            foreach ($activities as $activity) {
                $this->saveActivity($mrId, $activity, $student, $request->year_id);
            }
        }
    }

    private function saveActivity($mrId, $requestActivity, $student, $yearId)
    {
        $activity = Activity::query()
            ->with('years')
            ->where(['name' => $requestActivity['name']])
            ->where(['manage_reference_id' => $mrId])
            ->first();

        if (!$activity) {
            $activity = new Activity();
            $activity->name = $requestActivity['name'];
            $mr = ManageReference::query()->where(['id' => $mrId])->first();

            if (!$mr) {
                throw new \Exception('ManageReference' . ($mrId) . ' isn\'t exists');
            }

            $activity->manage_reference_id = $mrId;
            $activity->save();
        }

        $this->attachActivityToStudentWithPivotData($student, $activity, $requestActivity, $yearId);
    }

    private function attachActivityToStudentWithPivotData($student, Activity $activity, $requestActivity, $yearId)
    {
        $student->activities()->attach([
            $activity->id => [
                'value' => $requestActivity['value'],
                'count' => $requestActivity['count'],
                'comment' => $requestActivity['comment'],
                'year_id' => $yearId,
            ]
        ]);
    }

    public function getGraphs(Student $student)
    {
        $years = Year::query()->get();
        $activities = [];

        foreach ($years as $year) {
            $activities[] = DB::table('activities')
                ->selectRaw(DB::raw('manage_reference_id, color, SUM(students_activities.count*students_activities.value) as activities'))
                ->join(
                    'students_activities',
                    'students_activities.activity_id',
                    '=',
                    'activities.id',
                )
                ->join(
                    'manage_references',
                    'manage_references.id',
                    '=',
                    'activities.manage_reference_id',
                )
                ->where(['year_id' => $year->id])
                ->where(['student_id' => $student->id])
                ->groupBy('manage_reference_id')
                ->get();

        }

        return $activities;
    }

}
