<?php

namespace App\Listeners;

use Adldap\Auth\BindException;
use Adldap\Laravel\Events\AuthenticationFailed;
use Illuminate\Support\Facades\Log;

class LogAuthFailure
{
    public function handle(AuthenticationFailed $event)
    {
        if ($event->user->getBadPasswordCount() >= getenv('LDAP_BAD_PASSWORDS_COUNT')) {
            Log::info("User '{$event->user->getCommonName()}' has failed LDAP authentication.");

            throw new BindException('Your passwords attempts count over 5');
        }
    }
}
