<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestFacade;

abstract class ApiRequest extends FormRequest
{
    protected const AUTH_TOKEN_HEADER = 'Bearer 285024b61097cdcd034900c79fb761a2727f7726b16f7ce09d8c20c7e81c705dqwq';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)  {

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
