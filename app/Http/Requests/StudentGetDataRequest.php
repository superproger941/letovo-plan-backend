<?php

namespace App\Http\Requests;

class StudentGetDataRequest extends ApiRequest
{
    public function authorize()
    {
        // auth validate
        $inputAuthHeader = $this->header('Authorization');
        $isAuthHeaderWrong = (!$inputAuthHeader || ($inputAuthHeader != $this::AUTH_TOKEN_HEADER));

        return !$isAuthHeaderWrong;
    }

    public function rules()
    {
        return [
            'years_start' => 'required'
        ];
    }

    public function messages()
    {

        return [

        ];
    }
}
