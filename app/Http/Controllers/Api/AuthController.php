<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LdapLoginValidation;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $service;

    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    /**
     * @OA\Post(
     *     tags={"auth"},
     *     path="/api/auth/login",
     *     description="Login",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"username": "username", "password": "password"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="default",
     *          description="Auth user object",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="array",
     *                  @OA\Items(
     *                      type="string",
     *                      example="Login or password is incorrect"
     *                  ),
     *              )
     *          )
     *     ),
     *    @OA\Response(
     *          response=422,
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="The given data was invalid."
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="object",
     *                  @OA\Property(
     *                      property="username",
     *                      type="array",
     *                      @OA\Items(
     *                          type="string",
     *                          example="The username field is required."
     *                      ),
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="array",
     *                      @OA\Items(
     *                          type="string",
     *                          example="The password field is required."
     *                      ),
     *                  ),
     *              )
     *          )
     *     )
     * )
     */
    public function login(LdapLoginValidation $request)
    {
        return $this->service->login($request);
    }

    /**
     * @OA\Get(
     *     tags={"auth"},
     *     path="/api/auth/me",
     *     description="Get user info",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *          response="default",
     *          description="link",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Unauthenticated."),
     *          )
     *     )
     * )
     */
    public function user()
    {
        return [
            'data' => Auth::user()
        ];
    }
}
