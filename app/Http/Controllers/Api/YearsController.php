<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Year;

class YearsController extends Controller
{
    /**
     * @OA\Get(
     *     tags={"years"},
     *     path="/api/years",
     *     description="Get all avaliable years",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *          response="default",
     *          description="All avaliable years",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Unauthenticated."),
     *          )
     *     )
     * )
     */
    public function getAll()
    {
        return Year::all();
    }
}
