<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentGetDataRequest;
use App\Models\Student;
use App\Services\ActivitiesRepository;
use App\Services\StudentsRepository;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    private $studentsRepository;
    private $activitiesRepository;

    public function __construct(
        StudentsRepository $studentsRepository,
        ActivitiesRepository $activitiesRepository
    )
    {
        $this->studentsRepository = $studentsRepository;
        $this->activitiesRepository = $activitiesRepository;
    }

    /**
     * @OA\Get(
     *     tags={"students"},
     *     path="/api/students",
     *     description="Get all students",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *          response="default",
     *          description="Get all students",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Unauthenticated."),
     *          )
     *     )
     * )
     */
    public function getStudents(): string
    {
        return $this->studentsRepository->getStudents();
    }

    /**
     * @OA\Get(
     *     tags={"students"},
     *     path="/api/students/{studentId}",
     *     description="Get all students",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="studentId",
     *          in="path",
     *          description="student id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *          response="default",
     *          description="Get student by his id",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Unauthenticated."),
     *          )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Error: Not Found"
     *     )
     * )
     */
    public function findOne(Student $student): Student
    {
        return $student;
    }

    /**
     * @OA\Get(
     *     tags={"students"},
     *     path="/api/students/{studentId}/getPlanningPage",
     *     description="Get student' planning page",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="studentId",
     *          in="path",
     *          description="student id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="years_start",
     *         in="query",
     *         description="Year of activities"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Student' activities selected by year",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthenticated."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Error: Not Found"
     *     )
     * )
     */
    public function getPlanningPage(Student $student, StudentGetDataRequest $request): array
    {
        return $this
            ->activitiesRepository
            ->getRemoteActivitiesAndMergeWithLocal($student, $request->years_start);
    }

    /**
     * @OA\Get(
     *     tags={"students"},
     *     path="/api/students/getPlanningPageByAnalyticsId/{id}",
     *     description="Get student' planning page",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="analytics id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="years_start",
     *         in="query",
     *         required=true,
     *         description="Year of activities"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Student' activities selected by year",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthenticated."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Error: Not Found"
     *     )
     * )
     */
    public function getPlanningPageByAnalyticsId($id, StudentGetDataRequest $request): array
    {
        $student = Student::query()
            ->where(['analytics_id' => $id])
            ->first();

        if(empty($student)) {
            abort(404);
        }

        return $this
            ->activitiesRepository
            ->getRemoteActivitiesAndMergeWithLocal($student, $request->years_start);
    }

    /**
     * @OA\Post(
     *     tags={"students"},
     *     path="/api/students/{studentId}/saveActivities",
     *     description="Save student' activities",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="studentId",
     *          in="path",
     *          description="student id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="year_id",
     *         in="query",
     *         description="Year id which you store in database and can get in another request method",
     *         example="1"
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="activities",
     *                     type="string",
     *                     description="Array of five manage references appropriates their order in store includes arrays of activities"
     *                 ),
     *                 example={
     *                      "activities":
     *                          {
     *                              {
     *                                  {
     *                                      "id":7,
     *                                      "name":"Прошел отбор на соревнованиях I уровня",
     *                                      "source_type":"0",
     *                                      "score":"10.0",
     *                                      "medal_level":"Серебряные",
     *                                      "checked":true,
     *                                      "saved":false,
     *                                      "value":10,
     *                                      "count":1,
     *                                      "comment":""
     *                                  }
     *                              },
     *                              {},
     *                              {},
     *                              {},
     *                              {},
     *                          }
     *                      }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Nothing",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthenticated."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Error: Not Found"
     *     )
     * )
     */
    public function saveActivities(Student $student, Request $request)
    {
        $this->activitiesRepository->saveActivities($student, $request);
    }

    /**
     * @OA\Get(
     *     tags={"students"},
     *     path="/api/students/{studentId}/getGraphs",
     *     description="Get student' activities graphs",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="studentId",
     *          in="path",
     *          description="student id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *          response="default",
     *          description="Get student' activities graphs",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Unauthenticated."),
     *          )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Error: Not Found"
     *     )
     * )
     */
    public function getGraphs(Student $student)
    {
        return $this->activitiesRepository->getGraphs($student);
    }
}
