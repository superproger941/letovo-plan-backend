<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckStudentBelongsToUser
{
    /**
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function handle(Request $request, Closure $next)
    {
        $student = $request->route()->parameter('student');

        if($request->user()->id !== $student->user_id) {
            abort(401);
        }

        return $next($request);
    }
}
