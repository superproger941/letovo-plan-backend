<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Adldap\Laravel\Events\Authenticated' => [
            'App\Listeners\LogLdapAuthSuccessful',
        ],

        'Adldap\Laravel\Events\AuthenticationSuccessful' => [
            'App\Listeners\LogAuthSuccessful'
        ],

        'Adldap\Laravel\Events\AuthenticationFailed' => [
            'App\Listeners\LogAuthFailure',
        ],

        'Adldap\Laravel\Events\AuthenticationRejected' => [
            'App\Listeners\LogAuthRejected',
        ],
    ];

    public function boot()
    {
    }
}
